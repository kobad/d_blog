class ArticlesController < ApplicationController
  before_action :find_article, {only: [:edit,:show,:update,:destroy]}
  add_flash_types :success, :info, :warning, :danger
  
  def index
    @article=Article.order(created_at: :desc)
  end

  def show

  end

  def edit
  end

  def new
    @article=Article.new
  end


  def create
    @article=Article.new(article_params)
    if @article.save
      redirect_to @article, success: "作成しました"
    else
      flash.now[:danger]= "作成失敗"
      render :new 
    end
  end

  def update
    if @article.update(article_params)
      redirect_to @article, success: "更新しました"
    else
       falsh.now[:danger]= "更新失敗"
       render :edit
    end
  end

  def destroy
    if @article.destroy
       
      redirect_to root_path, success:"削除に成功"
    else
      flash.now[:danger]= "削除に失敗"
      redirect_to root_path 
    end
  end

  private

  def find_article
    @article=Article.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:title, :body)
  end

end
